﻿public class Generator {

    public static void Evolve(Cell[] cells)
    {
        int sz = cells.Length;
        
        for (int i = 0; i < sz; i++)
        {
            Cell left = null, right = null;
            if(i>0)
                 left = cells[i-1];
            if(i<sz-1)
                right = cells[i+1];

            Cell middle = cells[i];

            // Look up the new value according to the rules.
            ApplyRule(left, middle, right);            
        }
    }

    /**********************************************************************************
        * Rules:
        Every cell interacts with its two neighbours, 
        Score of 0 and 2 make alive, whereas 1 or 3 is dead (lonely or over-populated)
        -------------------------------------------------------------------------------------
        give this as an exercise:
        At each step in time, the following transitions occur: 
        * 
        * 111 110 101 100 011 010 001 000 
        *  v   v   v   v   v   v   v   v
        *  0   1   1   0   1   0   0   1
        **********************************************************************************/
     static void ApplyRule(Cell left, Cell me, Cell right)
     {
        int count = 0;
        if (left != null && left.AliveNow)
            count++;
        if (me.AliveNow)
            count++;
        if (right != null && right.AliveNow)
            count++;

        if(count == 0 || count == 2)
            me.AliveNext = true;
        else
            me.AliveNext = false;

    }

}
