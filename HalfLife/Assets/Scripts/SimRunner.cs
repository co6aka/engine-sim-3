using UnityEngine;
using UnityEngine.UI;

public class SimRunner : MonoBehaviour {

    public GameObject[ ] objects;
	public Text msgText;

	private int generation = 0;

    private bool running = false;
    private Cell [ ] cells;

    void Start()
    {
        cells = new Cell[objects.Length];

        for (int i = 0; i < objects.Length; i++)
            cells[i] = objects[i].GetComponent<Cell>();

        msgText.text = "Generation: 0";
    }

    public void Loop()
    {
        if (running)
            return;

        running = true;
        InvokeRepeating("Next", 1f, 1.5f);
    }

    // Can be called by a simulator, or manually via a button click
    public void Next () {

		msgText.text = "Generation: " + ++generation;
        Generator.Evolve(cells);
        foreach (Cell c in cells)
            c.Advance();
	}

}
