﻿using UnityEngine;

public class Cell : MonoBehaviour {

    public Sprite liveImg, deadImg;
    private SpriteRenderer sr;
    public bool AliveNext;

    public bool AliveNow {
        get { return sr.sprite == liveImg; }
        set {
            if (value == true)
                sr.sprite = liveImg;
            else
                sr.sprite = deadImg;
        }
    }

    void Start()
    {
        sr = GetComponent<SpriteRenderer>();
        AliveNow = false; // all cells start as dead
    }
   
    void OnMouseDown()      // flip live or dead
    {
        AliveNow = !AliveNow;
    }

    public void Advance()
    {
        AliveNow = AliveNext;
        AliveNext = false;
    }
}
