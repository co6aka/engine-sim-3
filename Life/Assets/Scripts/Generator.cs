﻿public class Generator {

    public static void Evolve(Cell[,] cells)
    {
        int rows = cells.GetLength(0);
        int cols = cells.GetLength(1);

        // we leave out borders, to avoid out of bounds exceptions
        // if our borderline cells are permanently dead, so be it
        for (int x = 1; x < rows - 1; x++)
            for (int y = 1; y < cols - 1; y++)
                ApplyRule3x3(cells, x, y);
         
    }

    /**********************************************************************************
     * Rules:
     Every cell interacts with its 8 neighbours
     Any live cell with two or three neighbors survives   
     Any dead cell with trhee neigbors awakens
     **********************************************************************************/
    static void ApplyRule3x3(Cell[,] population, int x, int y)
    {
        int count = 0;
        Cell myself = population[x, y];

        for (int i = x-1; i <= x+1; i++)
        {
            for (int j = y-1; j <= y+1; j++)
            {
                if (i == x & j == y)
                    continue;       // don't count yourself
                if (population[i, j].AliveNow)
                    count++;
            }
        }

        if (myself.AliveNow)
        {
            if (count < 2 || count > 3)
                myself.AliveNext = false;
            else
                myself.AliveNext = true;
        }
        else  // dead now: resurrects or not
            if (count == 3)
               myself.AliveNext = true;
            
    }

}
