using UnityEngine;

public class NewVehicleMine : MonoBehaviour
{
    Vector3 location, velocity, acceleration;
    float r = 3f;
    public float maxForce = 0.1f;
    public float maxSpeed = 4f;
    int i;

    public GameObject[] waypoints;
    GameObject nextWp;

    void Start()
    {
        location = transform.position;
        nextWp = waypoints[0];
    }

    // compute basic physics parameters
    void FixedUpdate()
    {
        velocity += acceleration * Time.deltaTime;
        if(velocity.magnitude > maxSpeed)   // limit max velocity
        {
            velocity = velocity.normalized;
            velocity *= maxSpeed;
        }

        location += velocity;
        acceleration = Vector3.zero;

        if((location - nextWp.transform.position).magnitude < 0.2f)
        {
            i++;
            if (i >= waypoints.Length)
                i = 0;
            nextWp = waypoints[i];
            print("going to " + nextWp.name);
        }
        Seek(nextWp.transform.position);
    }

    // Our seek steering force algorithm
    void Seek(Vector3 target)
    {
        Vector3 desired = target - location;
        float distance = desired.magnitude;

        desired = desired.normalized;

        float speedFactor = 1f;
        if (distance <= 5f)  // start slowing down
        {
            speedFactor = 0.5f;
        }

        desired *= (maxSpeed * speedFactor);
        
        Vector3 steer = desired - velocity;

        if(steer.magnitude > maxForce)
        {
            steer = steer.normalized;
            steer *= maxForce;
        }
        Debug.DrawRay(transform.position, steer, Color.green, 2f);
        ApplyForce(steer);
    }

    // Newton�s second law; we could divide by mass if we wanted.
    void ApplyForce(Vector3 force)
    {
        acceleration += force;
    }

    // refresh on screen
    void Update()
    {
        transform.position = location;
        transform.right = velocity; // hack?
    }
}
