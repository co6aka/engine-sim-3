﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// an eager vehicle that accelerates and slows down in arrival zone
public class Slick : Vehicle
{
    protected Vector2 location, velocity, acceleration;
    protected float maxforce = 0.1f;   // adjust as needed
    protected float maxspeed = 0.4f;

    void Start()
    {
        acceleration = Vector2.zero;
        velocity = Vector2.zero;
        location = transform.position;
    }

    void Update()
    {
        Vector3 point = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector2 target = new Vector2(point.x, point.y);

        NoseToTarget(target);
      //  Steer(target);
        MoveTo(target);
    }

    // Newton’s second law; we could divide by mass if we wanted.
    protected void ApplyForce(Vector2 force)
    {
        //acceleration += force;
        acceleration += force / 100f;  // for now
        velocity += acceleration;
        location = velocity;
        transform.position = location;
    }

    void MoveTo(Vector2 target)
    {
        Vector2 desired = target - location;

        float d = desired.magnitude;
        print("desired: " + desired + " d: " + d);

        desired.Normalize();

        //////////////// this is completely broken :( ////////////////
        // if we within an arrival circle, slow down..
        if (d < 1f) {
            // set the magnitude according to how close we are.
            float scale = d;
            desired *= scale;
        }
        else  //  proceed at maximum speed
            desired *= maxspeed;

        Vector2 steer = desired - velocity;
        Limit(steer, maxforce);
        ApplyForce(steer);
    }
}
