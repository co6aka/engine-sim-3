﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Chase the mouse, and slow down as you get closer
// no acceleration here, only the speed is controlled
public class FlatFollower : Vehicle
{
    float axforce = 0.4f;

    Vector2 velocity = Vector2.zero;
    Vector2 location = Vector2.zero;

    void Start()
    {
        transform.position = location;
    }

    void Update()
    {
        Vector2 mp = Input.mousePosition;     // screen coords
        Vector2 mousePos = Camera.main.ScreenToWorldPoint(
            new Vector3(mp.x, mp.y, Camera.main.nearClipPlane));    // world coords

        Vector2 dir = mousePos - location;

        velocity = dir * Time.deltaTime * 2.5f;

       // Limit(velocity, topspeed);
        location += velocity;

        transform.position = location;
        NoseToTarget(mousePos);
    }

}
