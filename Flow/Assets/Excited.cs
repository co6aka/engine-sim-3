﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// An accelerated mouse follower that gets too excited...
public class Excited : Vehicle
{
    protected Vector2 location, velocity, acceleration;
    // Additional variable for size
    float r;
    protected float maxforce = 0.1f;   // adjust as needed
    protected float maxspeed = 0.4f;

    void Start()
    {
        acceleration = Vector2.zero;
        velocity = Vector2.zero;
        location = transform.position;
        r = 3.0f;
    }

    // Newton’s second law; we could divide by mass if we wanted.
    protected void ApplyForce(Vector2 force)
    {
        //acceleration += force;
        acceleration += force / 100f;  // for now
    }

    // Our seek steering force algorithm
    protected void Steer(Vector2 target)
    {
        // now steer to it
        Vector2 desired = target - location;
        desired.Normalize();
        desired *= maxspeed;
        Vector2 steer = desired - velocity;
        Limit(steer, maxforce);
        ApplyForce(steer);
    }

    //  Our standard "Euler integration" motion model
    protected void Move()
    {
        velocity += acceleration;
        Limit(velocity, maxspeed);
        location += velocity;

        // print("acc: " + acceleration + " velocity: " + velocity);
        transform.position = location;
        acceleration = Vector2.zero;        // reset each frame
    }

    void Update()
    {
        Vector3 point = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector2 target = new Vector2(point.x, point.y);

        NoseToTarget(target);
        Steer(target);
        Move();
    }

}
