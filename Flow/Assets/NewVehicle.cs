using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static TMPro.SpriteAssetUtilities.TexturePacker_JsonArray;
using static UnityEngine.GraphicsBuffer;

public class NewVehicle : MonoBehaviour
{
    Vector3 location, velocity, acceleration;
    float r = 3f;
    public float maxForce = 0.2f;
    public float maxSpeed = 4f;
    public GameObject centerTrack;
    int i;

    public GameObject waypointsFolder;
    static List<GameObject> waypoints = new List<GameObject>();
    GameObject nextWp;
    GameObject LastWp;

    Vector2 a;
    void Start()
    {
        foreach (Transform child in waypointsFolder.transform)
        {
            waypoints.Add(child.gameObject);
            //sprint("adding" + child);
        }
        location = transform.position;
        LastWp = waypoints[waypoints.Count-1];
        nextWp = waypoints[0];
    }

    // compute basic physics parameters
    void FixedUpdate()
    {
        velocity += acceleration * Time.deltaTime*3;
        if(velocity.magnitude > maxSpeed)   // limit max velocity
        {
            velocity = velocity.normalized;
            velocity *= maxSpeed;
        }

        Debug.DrawLine(LastWp.transform.position, nextWp.transform.position);
      
        Vector3 P = transform.position;
        Vector3 w0 = LastWp.transform.position;
        Vector3 w1 = nextWp.transform.position;

        Vector3 projectedPoint = Vector3.Project((P - w0), (w1 - w0)) + w0;
        centerTrack.transform.position = projectedPoint;


        location += velocity;
        acceleration = Vector3.zero;

        if((location - nextWp.transform.position).magnitude < 0.5f)
        {
            LastWp = waypoints[i];
            i++;
            if (i >= waypoints.Count)
                i = 0;
            nextWp = waypoints[i];
            //print("going to " + nextWp.name);
        }
        Seek(nextWp.transform.position);
    }

    // Our seek steering force algorithm
    void Seek(Vector3 target)
    {
        Vector3 desired = target - location;
        float distance = desired.magnitude;

        desired = desired.normalized;

        float speedFactor = 1f;
        if (distance <= 5f)  // start slowing down
        {
            speedFactor = 0.5f;
        }

        desired *= (maxSpeed * speedFactor);
        
        Vector3 steer = desired - velocity;

        if(steer.magnitude > maxForce)
        {
            steer = steer.normalized;
            steer *= maxForce;
        }
        Debug.DrawRay(transform.position, steer, Color.green, 2f);
        ApplyForce(steer);
    }

    // Newton�s second law; we could divide by mass if we wanted.
    void ApplyForce(Vector3 force)
    {
        acceleration += force;
    }

    // refresh on screen
    void Update()
    {
        transform.position = location;
        transform.right = velocity; // hack?
    }
}
