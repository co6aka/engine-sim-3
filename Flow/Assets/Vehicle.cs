﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// base class that manages orientation and speed limit
public class Vehicle : MonoBehaviour
{
    protected void Limit(Vector2 v, float max)
    {
        if (v.magnitude < max)
            return;

        // reset to unit vector and adjust for top speed
        v = v.normalized * max;
    }

    protected void NoseToTarget(Vector2 target)
    {
        // let's cheat for now:
        transform.right = target - new Vector2(transform.position.x, transform.position.y);
        // 3D only: transform.LookAt(target);
    }
}
