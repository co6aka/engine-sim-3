﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// place objects in two corners, alternating up and down
public class Square : MonoBehaviour
{
    public GameObject prefab;

    [Range(0.5f, 3f)]
    public float startSize = 1f;

    void Start()
    {
        CreateItem(0f, 0f, startSize);
    }

    void CreateItem(float x, float y, float len)
    {
        Place(x, y, len);

        if (len < 0.2f)
            return;         // base condition
        
        CreateItem(x + len * 1.3f, y + len * 1.3f, len / 2f); 
        CreateItem(x - len * 1.3f, y - len * 1.3f, len / 2f);
        CreateItem(x - len * 1.3f, y + len * 1.3f, len / 2f);
        CreateItem(x + len * 1.3f, y - len * 1.3f, len / 2f);
    }

    void Place(float x, float y, float scale)
    {
        Vector2 location = new Vector2(x, y);
        GameObject go = (GameObject)GameObject.Instantiate(this.prefab, location, Quaternion.identity);
        go.transform.localScale = new Vector2(scale, scale);
    }

}
