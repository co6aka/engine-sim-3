﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Circle : MonoBehaviour
{
    public GameObject prefab;

    [Range(0.5f, 3f)]
    public float startRadius = 1f;

    void Start()
    {
        CreateItem(0f, 0f, startRadius);
    }

    void CreateItem(float x, float y, float r)
    {
        Place(x, y, r);

        if (r < 0.1f)
            return;         // our base case

        CreateItem(x + r *2f, y, r / 2f);
        CreateItem(x - r *2f, y, r / 2f);
    }

    void Place(float x, float y, float scale)
    {
        Vector2 location = new Vector2(x, y);
        GameObject go = (GameObject)GameObject.Instantiate(this.prefab, location, Quaternion.identity);
        go.transform.localScale = new Vector2(scale, scale);
    }

}
