﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// place objects in two corners, alternating up and down
public class Tree : MonoBehaviour
{
    public GameObject prefab;

    void Start()
    {
        CreateItem(0f, 0f, 4.0f, 0f);
    }

    void CreateItem(float x, float y, float len, float rotation)
    {
        Place(x, y, len, rotation);

        if (len < 0.2f)
            return;

        CreateItem(x-len/8f, y + len/2f, len / 2f, rotation+30);    // rotation is steeper with next generations
        CreateItem(x+len/8f, y + len/2f, len / 2f, rotation-30);
    }

    void Place(float x, float y, float scale, float degrees)
    {
        Vector2 location = new Vector2(x, y);
        GameObject go = (GameObject)GameObject.Instantiate(this.prefab, location, Quaternion.identity);
        go.transform.Rotate(0f, 0f, degrees);
        go.transform.localScale = new Vector2(scale, scale);
    }

}
