﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cubanizer : MonoBehaviour
{
    public GameObject prefab;
    public float offset = 1.25f;

    void Start()
    {
        CreateItem(prefab, 0f, 0f, 0f, 1.0f);
    }

    void CreateItem(GameObject prefab, float x, float y, float z, float len)
    {
        Place(prefab, x, y, z, len);
        if (len > 0.2f)
        {
            // very cool:
            CreateItem(prefab, x + len * offset, y + len * offset, z, len / 2f);
            CreateItem(prefab, x - len * offset, y - len * offset, z, len / 2f);
            CreateItem(prefab, x - len * offset, y + len * offset, z, len / 2f);
            CreateItem(prefab, x + len * offset, y - len * offset, z, len / 2f);

            /* not bad, but too busy:
            CreateItem(prefab, x + len * offset, y + len * offset, z + len * offset, len / 2f);
            CreateItem(prefab, x - len * offset, y - len * offset, z + len * offset, len / 2f);
            CreateItem(prefab, x - len * offset, y + len * offset, z + len * offset, len / 2f);
            CreateItem(prefab, x + len * offset, y - len * offset, z + len * offset, len / 2f);
            CreateItem(prefab, x + len * offset, y + len * offset, z - len * offset, len / 2f);
            CreateItem(prefab, x - len * offset, y - len * offset, z - len * offset, len / 2f);
            CreateItem(prefab, x - len * offset, y + len * offset, z - len * offset, len / 2f);
            CreateItem(prefab, x + len * offset, y - len * offset, z - len * offset, len / 2f);
            */
        }
    }

    void Place(GameObject o, float x, float y, float z, float scale)
    {
        Vector3 location = new Vector3(x, y, z);
        GameObject go = (GameObject)GameObject.Instantiate(o, location, Quaternion.identity);
        go.transform.localScale = new Vector3(scale, scale, scale);
    }

}
